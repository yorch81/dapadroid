# DapaDroid #

## Descripción ##
DapaDroid es una aplicación Android para la toma de lecturas de agua potable para la
Dirección de Agua Potable y Alcantarillado de Ciudad Valles, S.L.P.

## Requerimientos ##
* [Java](https://www.java.com/es/download/)
* [Android Studio](https://developer.android.com/sdk/index.html)
* [Android Bootstrap](https://github.com/Bearded-Hen/Android-Bootstrap)
* [LoopJ](http://loopj.com/android-async-http/)

## Actualización ##
* [GitLab](https://gitlab.com/yorch81/dapadroid)

## Documentación ##
Se puede generar desde Android Studio.

## Instalación ##
La configuración de Android por default solo permite instalar aplicaciones desde Google Play, para poder instalar el archivo apk generado deberá activar la instalación de Origenes Desconocidos (Unknow Sources) en el celular anteriormente.

## Notas ##
Esta aplicación se enlaza, mediante MacREST (webservice), al Sistema Atlantus que es el que se usa en la DAPA para llevar a cabo todos los procesos.

El código de MacREST no puede distribuirse debido a que accede a información pública.

DapaDroid es un donativo para el personal de informática que labora en DAPA.  

## Referencias ##
http://developer.android.com/design/index.html

## Licencia ##
http://www.apache.org/licenses/LICENSE-2.0

P.D. Let's go play !!!