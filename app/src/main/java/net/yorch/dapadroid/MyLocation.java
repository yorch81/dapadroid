package net.yorch.dapadroid;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * LectLocation<br>
 *
 * LectLocation Manage Location of Device<br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0.0, 2015-09-08
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class MyLocation implements LocationListener {
    /**
     * NETWORK
     *
     * VAR String NETWORK Network Adapter
     */
    public static final String NETWORK = LocationManager.NETWORK_PROVIDER;

    /**
     * GPS
     *
     * VAR String GPS GPS Adapter
     */
    public static final String GPS = LocationManager.GPS_PROVIDER;

    /**
     * BEST
     *
     * VAR String BEST Best Adapter
     */
    public static final String BEST = "best";

    /**
     * LOGTAG
     *
     * VAR String LOGTAG Log Identificator
     */
    private static final String LOGTAG = "LogsLectLocation";

    /**
     * locManager
     *
     * VAR LocationManager locManager Manager of Location
     */
    private LocationManager locManager;

    /**
     * provider
     *
     * VAR String provider Selected Provider
     */
    private String provider = "";

    /**
     * appContext
     *
     * VAR Context appContext Application Context
     */
    private Context appContext =  null;

    /**
     * Last Latitude
     *
     * VAR double latitude
     */
    private double latitude  = 0;

    /**
     * Last Longitude
     *
     * VAR double longitude
     */
    private double longitude = 0;

    /**
     * Time for refresh Location
     *
     * VAR int time
     */
    private int time = 0;

    /**
     * Distance for refresh Location
     *
     * VAR int distance
     */
    private int distance = 0;

    /**
     * Constructor LectLocation
     *
     * @param context Context Application Context
     * @param locProvider String Selected Provider of Location
     */
    public MyLocation(Context context, String locProvider){
        appContext = context;
        provider = locProvider;
    }

    /**
     * Constructor LectLocation
     *
     * @param context Context Application Context
     * @param locProvider String Selected Provider of Location
     * @param pTime int Time for refresh Location
     * @param pDistance int Distance for refresh Location
     */
    public MyLocation(Context context, String locProvider, int pTime, int pDistance){
        appContext = context;
        provider = locProvider;
        time = pTime;
        distance = pDistance;
    }

    /**
     * Start Service Location
     */
    public void start(){
        locManager =  (LocationManager)appContext.getSystemService(Context.LOCATION_SERVICE);

        if (provider == BEST){
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            provider = locManager.getBestProvider(criteria, false);

            criteria = null;
        }

        if (!locManager.isProviderEnabled(provider)){
            Log.e(LOGTAG, "Provider Disabled");
        }
        else {
            Location location =  locManager.getLastKnownLocation(provider);

            try{
                this.setLatitude(location.getLatitude());
                this.setLatitude(location.getLatitude());
            }
            catch (Exception e) {
                this.setLatitude(0);
                this.setLatitude(0);
            }

            locManager.requestLocationUpdates(provider, time, distance, this);
        }
    }

    /**
     * Stop Service Location
     */
    public void stop(){
        try{
            locManager.removeUpdates(this);
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Can't Remove listener", e);
        }
    }

    /**
     * Set Latitude
     *
     * @param latitude double Latitude
     */
    private void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * Set Longitude
     *
     * @param longitude double Longitude
     */
    private void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * Get Last Latitude
     *
     * @return double
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Get Last Longitude
     *
     * @return double
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Get Time for refresh Location
     *
     * @return int
     */
    public int getTime() {
        return time;
    }

    /**
     * Set Time for refresh Location
     *
     * @param time int Time
     */
    public void setTime(int time) {
        this.time = time;
    }

    /**
     * Get Distance for refresh Location
     *
     * @return int
     */
    public int getDistance() {
        return distance;
    }

    /**
     * Set Distance for refresh Location
     *
     * @param distance int Distance
     */
    public void setDistance(int distance) {
        this.distance = distance;
    }

    /**
     * Method to refresh Location
     *
     * @param location Location
     */
    @Override
    public void onLocationChanged(Location location) {
        Log.e(LOGTAG, "Location Changed");
        this.setLatitude(location.getLatitude());
        this.setLongitude(location.getLongitude());
    }

    /**
     * Implement Method onProviderDisabled
     *
     * @param provider String
     */
    @Override
    public void onProviderDisabled(String provider) {
        this.setLatitude(0);
        this.setLongitude(0);
        Log.e(LOGTAG, provider + " OFF");
    }

    /**
     * Implement Method onProviderEnabled
     *
     * @param provider String
     */
    @Override
    public void onProviderEnabled(String provider) {
        Log.e(LOGTAG, provider + " OFF");
    }

    /**
     * Implement Method onStatusChanged
     *
     * @param provider String
     * @param status int
     * @param extras Bundle
     */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i(LOGTAG, provider + " Status: " + String.valueOf(status));
    }
}
