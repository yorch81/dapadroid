package net.yorch.dapadroid;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * myCURL<br>
 *
 * myCURL Manage get and post request to WebService<br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0.0, 2015-09-08
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class myCURL {
    /**
     * LOGTAG
     *
     * VAR String LOGTAG Log Identificator
     */
    private static final String LOGTAG = "myCURL";

    /**
     * status
     *
     * VAR int status Status Code of Request
     */
    private int status = 0;

    /**
     * response
     *
     * VAR String response Response of Request
     */
    private String response = "";

    /**
     * url
     *
     * VAR String url URL of WebService
     */
    private String url = "";

    /**
     * data
     *
     * VAR String data Data to Send by POST
     */
    private String data = "";

    /**
     * varName
     *
     * VAR String varName Variable Name of data to send
     */
    private String varName = "";

    /**
     * Constructor of myCURL
     *
     * @param url String URL of WebService
     */
    public myCURL(String url) {
        this.url = url;

    }

    /**
     * Get Status of Last Request
     *
     * @return int
     */
    public int getStatus() {
        return status;
    }

    /**
     * Get Response of Last Request
     *
     * @return String
     */
    public String getResponse() {
        return response;
    }

    /**
     * Set other URL
     *
     * @param url String New URL
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Set POST Data to Send
     *
     * @param varName String Name of Variable to Send
     * @param data String Data to Send
     */
    public void setPostData(String varName, String data) {
        this.varName = varName;
        this.data = data;
    }

    /**
     * Execute GET Request
     *
     * @param key String Session Key to autenticate if necessary
     */
    public void get(String key){
        HttpClient httpclient = new DefaultHttpClient();
        response = "";
        status = -1;

        try{
            if (!key.isEmpty()){
                this.url = this.url + "?key=" + key;
            }

            HttpGet httpget = new HttpGet(this.url);
            HttpResponse httpResponse = httpclient.execute(httpget);

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            this.status = httpResponse.getStatusLine().getStatusCode();

            String line = "";
            while ((line = rd.readLine()) != null){
                this.response = this.response + line;
            }
        }
        catch (ClientProtocolException e){
            Log.e(LOGTAG, "ClientProtocolException", e);
            status = -1;
        }
        catch (IOException e){
            Log.e(LOGTAG, "IOException", e);
            status = -1;
        }
    }

    /**
     * Execute POST Request
     *
     * @param key String Session Key to autenticate if necessary
     */
    public void post(String key){
        if (key.length() > 0){
            this.url = this.url + "?key=" + key;
        }

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(this.url);
        response = "";
        status = -1;

        try{
            if (this.data.length() > 0){
                List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair(this.varName, this.data));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            }

            HttpResponse httpResponse = httpclient.execute(httppost);

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            this.status = httpResponse.getStatusLine().getStatusCode();

            String line = "";
            while ((line = rd.readLine()) != null){
                this.response = this.response + line;
            }
        }
        catch (ClientProtocolException e){
            Log.e(LOGTAG, "ClientProtocolException", e);
            status = -1;
        }
        catch (IOException e){
            Log.e(LOGTAG, "IOException", e);
            status = -1;
        }
    }
}
