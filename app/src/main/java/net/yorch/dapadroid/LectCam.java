package net.yorch.dapadroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.apache.http.Header;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * LectCam<br>
 *
 * Activity para Tomar Foto, Subir fotos al Server y Sincronizar Fotos.<br><br>
 * http://developer.android.com/training/camera/photobasics.html<br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0.0, 2015-09-08
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class LectCam extends AppCompatActivity {

    /**
     * LOGTAG
     *
     * VAR String LOGTAG Log Identificator
     */
    private static final String LOGTAG = "LogsLectCam";

    private static final int ACTION_TAKE_PHOTO_B = 1;
    private static final int ACTION_TAKE_PHOTO_S = 2;
    private static final int ACTION_TAKE_VIDEO = 3;

    private static final String BITMAP_STORAGE_KEY = "viewbitmap";
    private static final String IMAGEVIEW_VISIBILITY_STORAGE_KEY = "imageviewvisibility";
    private ImageView mImageView;
    private Bitmap mImageBitmap;

    private static final String VIDEO_STORAGE_KEY = "viewvideo";
    private static final String VIDEOVIEW_VISIBILITY_STORAGE_KEY = "videoviewvisibility";
    private VideoView mVideoView;
    private Uri mVideoUri;

    private String mCurrentPhotoPath;

    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";

    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

    private int folio = 0;

    private int cuenta = 0;

    /* Photo album for this application */
    private String getAlbumName() {
        return getString(R.string.album_name);
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

            if (storageDir != null) {
                if (! storageDir.mkdirs()) {
                    if (! storageDir.exists()){
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private void setPic() {

		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

		/* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

		/* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        }

		/* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

		/* Associate the Bitmap to the ImageView */
        mImageView.setImageBitmap(bitmap);
        mVideoUri = null;
        mImageView.setVisibility(View.VISIBLE);
        mVideoView.setVisibility(View.INVISIBLE);
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void dispatchTakePictureIntent(int actionCode) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        switch(actionCode) {
            case ACTION_TAKE_PHOTO_B:
                File f = null;

                try {
                    f = setUpPhotoFile();
                    mCurrentPhotoPath = f.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                } catch (IOException e) {
                    e.printStackTrace();
                    f = null;
                    mCurrentPhotoPath = null;
                }
                break;

            default:
                break;
        } // switch

        startActivityForResult(takePictureIntent, actionCode);
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(takeVideoIntent, ACTION_TAKE_VIDEO);
    }

    private void handleSmallCameraPhoto(Intent intent) {
        Bundle extras = intent.getExtras();
        mImageBitmap = (Bitmap) extras.get("data");
        mImageView.setImageBitmap(mImageBitmap);
        mVideoUri = null;
        mImageView.setVisibility(View.VISIBLE);
        mVideoView.setVisibility(View.INVISIBLE);
    }

    private void handleBigCameraPhoto() {

        if (mCurrentPhotoPath != null) {
            //setPic();
            galleryAddPic();

            // Upload Pic or Save
            final String picPath = mCurrentPhotoPath;

            AlertDialog.Builder confirmDlg = new AlertDialog.Builder(this);
            confirmDlg.setTitle("DapaDroid");
            confirmDlg.setMessage("¿Desea subir la Foto?");
            confirmDlg.setCancelable(true);
            confirmDlg.setNegativeButton("Cancelar",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            savePicture(picPath);
                        }
                    });
            confirmDlg.setPositiveButton(
                    "Aceptar",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Subir Foto
                            uploadPhoto(picPath, false);
                        }
                    });

            confirmDlg.show();

            mCurrentPhotoPath = null;
        }

    }

    private void handleCameraVideo(Intent intent) {
        mVideoUri = intent.getData();
        mVideoView.setVideoURI(mVideoUri);
        mImageBitmap = null;
        mVideoView.setVisibility(View.VISIBLE);
        mImageView.setVisibility(View.INVISIBLE);
    }

    Button.OnClickListener mTakePicOnClickListener =
            new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dispatchTakePictureIntent(ACTION_TAKE_PHOTO_B);
                }
            };

    Button.OnClickListener mTakePicSOnClickListener =
            new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dispatchTakePictureIntent(ACTION_TAKE_PHOTO_S);
                }
            };

    Button.OnClickListener mTakeVidOnClickListener =
            new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dispatchTakeVideoIntent();
                }
            };

    /**
     * Save Picture for Syncronize
     *
     * @param picPath String Picture Path
     */
    private void savePicture (String picPath){
        if (folio == 0){
            ReportesDb objReporte = new ReportesDb(getApplicationContext());
            objReporte.savePicCta(cuenta, picPath);
            objReporte = null;
        }
        else{
            ReportesDb objReporte = new ReportesDb(getApplicationContext());
            objReporte.savePic(folio, picPath);
            objReporte = null;
        }
    }
    /**
     * Upload Picture
     *
     * @param imagePath Ruta de la Foto
     * @param isSync    Indica si esta sincronizando
     */
    private void uploadPhoto(final String imagePath, final boolean isSync){
        try{
            SyncHttpClient client = new SyncHttpClient();
            RequestParams params = new RequestParams();

            params.put("cam", new File(imagePath));

            String url = "";

            // Si se manda cuenta
            if (folio == 0){
                url = DapaDroid.base_url + "/upload/cta/" + String.valueOf(cuenta);
            }
            else{
                url = DapaDroid.base_url + "/upload/rep/" + String.valueOf(folio);
            }

            // Execute POST picture
            client.post(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                    if (! isSync){
                        Toast.makeText(getApplicationContext(), "Imagen subida", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                    if (! isSync){
                        Toast.makeText(getApplicationContext(), "Hubo un Error al subir la Imagen", Toast.LENGTH_SHORT).show();
                        savePicture(imagePath);
                    }
                }
            });
        }
        catch (FileNotFoundException e){
            Log.e(LOGTAG, "Exception", e);
            e.printStackTrace();
        }
    }

    /**
     * Sincronizar Fotos
     */
    private void syncPics(){
        // Pictures
        final ReportesDb objReporte = new ReportesDb(getApplicationContext());

        final Cursor cPictures = objReporte.getPicSync();

        final Cursor cPicturesCta = objReporte.getPicSyncCta();

        int total = cPictures.getCount() + cPicturesCta.getCount();

        if (total > 0) {
            // Si faltan reportes por sincronizar
            if (objReporte.reportesXSync()) {
                Toast.makeText(getApplicationContext(), "Debe sincronizar primero los reportes, ejecute este proceso posteriormente", Toast.LENGTH_LONG).show();

                finish();
            }
            else {
                AlertDialog.Builder confirmDlg = new AlertDialog.Builder(this);
                confirmDlg.setTitle("DapaDroid");
                confirmDlg.setMessage("¿Desea sincronizar " + String.valueOf(total) + " fotos?");
                confirmDlg.setCancelable(true);
                confirmDlg.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Finish Activity
                        finish();
                    }
                });
                confirmDlg.setPositiveButton(
                        "Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                final ProgressDialog ringProgressDialog = ProgressDialog.show(LectCam.this, "Espere por favor ...", "Sincronizando Fotos ...", true);
                                ringProgressDialog.setCancelable(false);

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            // Fotos Reportes
                                            if (cPictures.getCount() > 0){
                                                cPictures.moveToFirst();

                                                do {
                                                    int id_picture = cPictures.getInt(0);
                                                    int id_reporte = cPictures.getInt(1);
                                                    String picture = cPictures.getString(2);

                                                    folio = id_reporte;

                                                    uploadPhoto(picture, true);

                                                    objReporte.updatePic(id_picture);
                                                } while (cPictures.moveToNext());
                                            }

                                            // Fotos Lecturas
                                            if (cPicturesCta.getCount() > 0){
                                                cPicturesCta.moveToFirst();

                                                do {
                                                    int id_picture = cPicturesCta.getInt(0);
                                                    int id_cuenta = cPicturesCta.getInt(1);
                                                    String picture = cPicturesCta.getString(2);

                                                    folio = 0;
                                                    cuenta = id_cuenta;

                                                    uploadPhoto(picture, true);

                                                    objReporte.updatePicCta(id_picture);
                                                } while (cPicturesCta.moveToNext());
                                            }
                                        } catch (Exception e) {
                                            Log.e(LOGTAG, "Exception", e);
                                        }

                                        ringProgressDialog.dismiss();

                                        // finish Activity
                                        finish();
                                    }
                                }).start();
                            }
                        });

                confirmDlg.show();
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "No existen fotos para Sincronizar", Toast.LENGTH_SHORT).show();

            finish();
        }
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.lyt_lectcam);

        mImageBitmap = null;
        mVideoUri = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }

        // Gets Parameters
        Intent iExtras = getIntent();

        Bundle bExtras = iExtras.getExtras();

        if (bExtras.containsKey("folio"))
            folio = bExtras.getInt("folio");

        if (bExtras.containsKey("cuenta")){
            cuenta = bExtras.getInt("cuenta");
            folio = 0;
        }

        if (bExtras.containsKey("sync"))
            syncPics();
        else
            dispatchTakePictureIntent(ACTION_TAKE_PHOTO_B);
    }

    /**
     * Show Camera on Press Button
     *
     * @param view
     */
    public void showCam(View view){
        dispatchTakePictureIntent(ACTION_TAKE_PHOTO_B);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTION_TAKE_PHOTO_B: {
                if (resultCode == RESULT_OK) {
                    handleBigCameraPhoto();
                }
                break;
            } // ACTION_TAKE_PHOTO_B

            case ACTION_TAKE_PHOTO_S: {
                if (resultCode == RESULT_OK) {
                    handleSmallCameraPhoto(data);
                }
                break;
            } // ACTION_TAKE_PHOTO_S

            case ACTION_TAKE_VIDEO: {
                if (resultCode == RESULT_OK) {
                    handleCameraVideo(data);
                }
                break;
            } // ACTION_TAKE_VIDEO
        } // switch
    }

    // Some lifecycle callbacks so that the image can survive orientation change
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(BITMAP_STORAGE_KEY, mImageBitmap);
        outState.putParcelable(VIDEO_STORAGE_KEY, mVideoUri);
        outState.putBoolean(IMAGEVIEW_VISIBILITY_STORAGE_KEY, (mImageBitmap != null) );
        outState.putBoolean(VIDEOVIEW_VISIBILITY_STORAGE_KEY, (mVideoUri != null) );
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mImageBitmap = savedInstanceState.getParcelable(BITMAP_STORAGE_KEY);
        mVideoUri = savedInstanceState.getParcelable(VIDEO_STORAGE_KEY);
        mImageView.setImageBitmap(mImageBitmap);
        mImageView.setVisibility(
                savedInstanceState.getBoolean(IMAGEVIEW_VISIBILITY_STORAGE_KEY) ?
                        ImageView.VISIBLE : ImageView.INVISIBLE
        );
        mVideoView.setVideoURI(mVideoUri);
        mVideoView.setVisibility(
                savedInstanceState.getBoolean(VIDEOVIEW_VISIBILITY_STORAGE_KEY) ?
                        ImageView.VISIBLE : ImageView.INVISIBLE
        );
    }

    /**
     * Indicates whether the specified action can be used as an intent. This
     * method queries the package manager for installed packages that can
     * respond to an intent with the specified action. If no suitable package is
     * found, this method returns false.
     * http://android-developers.blogspot.com/2009/01/can-i-use-this-intent.html
     *
     * @param context The application's environment.
     * @param action The Intent action to check for availability.
     *
     * @return True if an Intent with the specified action can be sent and
     *         responded to, false otherwise.
     */
    public static boolean isIntentAvailable(Context context, String action) {
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent(action);
        List<ResolveInfo> list =
                packageManager.queryIntentActivities(intent,
                        PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private void setBtnListenerOrDisable(
            Button btn,
            Button.OnClickListener onClickListener,
            String intentName
    ) {
        if (isIntentAvailable(this, intentName)) {
            btn.setOnClickListener(onClickListener);
        } else {
            btn.setText(
                    getText(R.string.cannot).toString() + " " + btn.getText());
            btn.setClickable(false);
        }
    }


}
