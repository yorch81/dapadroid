package net.yorch.dapadroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;

/**
 * DapaDroid<br>
 *
 * DapaDroid clase principal de la app, carga vistas, maneja eventos <br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0.0, 2015-09-08
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class DapaDroid extends AppCompatActivity {
    /**
     * LOGTAG
     * <p/>
     * VAR String LOGTAG Log Identificator
     */
    private static final String LOGTAG = "LogsDapaDroid";

    /**
     * cRuta
     * <p/>
     * VAR Cursor cRuta Cursor de la Ruta
     */
    Cursor cRuta = null;

    /**
     * base_url
     * <p/>
     * VAR String base_url URL base del WebService
     */
    public static String base_url = "";
    ;

    /**
     * key
     * <p/>
     * VAR String key Llave de Acceso al WebService
     */
    public static String key = "NO_KEY";

    /**
     * location
     * <p/>
     * VAR MyLocation location Objeto Localizacion
     */

    MyLocation location = null;

    /**
     * appCfg
     * <p/>
     * VAR appCfg Configuracion de la App
     */
    ConfigDb appCfg = null;


    /**
     * sectorActual
     * <p/>
     * VAR int Sector Actual
     */
    int sectorActual = 0;

    /**
     * Current Position
     * <p/>
     * VAR int currentPos
     */
    public static int currentPos = 0;

    /**
     * Manzana
     * <p/>
     * VAR int Manzana
     */
    public static int manzana = 0;

    /**
     * Medidor a buscar
     * <p/>
     * VAR String medidor
     */
    public static String medidor = "";

    /**
     * Cuenta que lanza reporte
     * <p/>
     * VAR int cuentaRep
     */
    public static int cuentaRep = 0;

    /**
     * folio
     * <p/>
     * VAR int folio Folio de reporte
     */
    public static int folio = 0;

    /**
     * cuenta
     * <p/>
     * VAR int cuenta Cuenta Actual
     */
    public static int cuenta = 0;

    /**
     * changeURL
     * <p/>
     * VAR boolean changeURL Indica si se prentende cambiar la URL
     */
    public static boolean changeURL = false;

    /**
     * offline
     * <p/>
     * VAR int offline Indica si la aplicacion esta funcionando fuera de linea
     */
    public static boolean offline = false;

    /**
     * Evento OnCreate
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Vertical
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Resuelve error en Android 4.0
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Inicializar Variables
        appCfg = new ConfigDb(getApplicationContext());
        base_url = appCfg.getURL();
        key = appCfg.getKey();
        currentPos = appCfg.getCurrent();

        // Iniciar localización
        if (location == null) {
            location = new MyLocation(getApplicationContext(), MyLocation.NETWORK, 30000, 10);
            location.start();
        }

        if (Login.ping(base_url)) {
            int status = Login.validateKey(key);

            if (status == 200) {
                showMenu();
            } else
                showLogin();
        } else {
            // Si existe una llave de sesión entrar
            if (key.equals("NO_KEY"))
                showURL();
            else{
                offline = true;

                Toast.makeText(getApplicationContext(), "DAPADroid se encuentra trabajando offline, posteriormente debe sincronizar todas las operaciones", Toast.LENGTH_LONG).show();

                showMenu();
            }
        }
    }

    /**
     * Carga la vista anterior al presionar BACK
     *
     * @return boolean
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Navigator.pop(this);

            if (cuentaRep > 0) {
                cuentaRep = 0;

                loadAnomalias();
                mostrar();
            }

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    /**
     * Show URL View
     */
    private void showURL() {
        setTitle("Configurar URL");
        setContentView(R.layout.lyt_config);

        EditText txtUrl = (EditText) findViewById(R.id.txtCfgUrl);

        txtUrl.setText(DapaDb.DEFAULT_URL);
    }

    /**
     * Cambia la URL
     *
     * @param view View
     */
    public void changeURL(View view) {
        Navigator.push(this, R.layout.lyt_config);

        EditText txtUrl = (EditText) findViewById(R.id.txtCfgUrl);

        txtUrl.setText(DapaDb.DEFAULT_URL);

        changeURL = true;
    }

    /**
     * Finalizar Ruta
     * Marca por sincronizar las lecturas no capturadas, con lectura 0
     * y observaciones 'DAPADROID: no pudo capturar la lectura'
     *
     * @param view View
     */
    public void finalizarRuta(View view) {
        Toast.makeText(getApplicationContext(), "Finalizando Ruta", Toast.LENGTH_LONG).show();

        LecturaDb objLect = new LecturaDb(getApplicationContext());

        objLect.finalizar();

        objLect = null;
    }

    /**
     * Checa si la URL es valida y manda al login
     *
     * @param view View
     */
    public void cfgConfigurar(View view) {
        EditText txtUrl = (EditText) findViewById(R.id.txtCfgUrl);

        String url = "http://" + txtUrl.getText().toString().trim();

        if (Login.ping(url)) {
            if (changeURL){
                ConfigDb cfg =  new ConfigDb(getApplicationContext());
                cfg.setURL(url);
                cfg.setCurrent(currentPos);

                // Finish App
                Navigator.pop(this);
                Navigator.pop(this);

                offline = false;

                Toast.makeText(getApplicationContext(), "Saliendo de DapaDroid", Toast.LENGTH_SHORT).show();

                finish();
            }
            else {
                base_url = url;

                showLogin();
            }
        } else {
            Toast.makeText(getApplicationContext(), "No se puede acceder al WebService", Toast.LENGTH_SHORT).show();
            txtUrl.setText("");
        }

        url = null;
    }

    /**
     * Show Login View
     */
    private void showLogin() {
        setTitle("Iniciar Sesión");
        setContentView(R.layout.lyt_login);
    }

    /**
     * Checa si usuario y password son validos
     *
     * @param view View
     */
    public void logLogin(View view) {
        EditText txtUser = (EditText) findViewById(R.id.txtLogUsuario);
        EditText txtPassword = (EditText) findViewById(R.id.txtLogPassword);

        key = Login.login(txtUser.getText().toString().trim(), txtPassword.getText().toString());

        if (key.equals("NO_KEY")) {
            Toast.makeText(getApplicationContext(), "Usuario o Password no validos", Toast.LENGTH_SHORT).show();
        } else {
            appCfg.update(base_url, key, currentPos, txtUser.getText().toString().trim());

            showMenu();
        }

        txtUser = null;
        txtPassword = null;
    }

    /**
     * Show Menu View
     */
    private void showMenu() {
        setTitle("DapaDroid");
        Navigator.push(this, R.layout.lyt_menu);
    }

    /**
     * Carga Combo de Anomalias
     */
    private void loadAnomalias() {
        // Cargar Combo
        LecturaDb objLect = new LecturaDb(getApplicationContext());

        Spinner cmbAnomalias = (Spinner) findViewById(R.id.cmbAnomalias);

        String[] from = new String[]{"descripcion"};
        int[] to = new int[]{android.R.id.text1};

        @SuppressWarnings("deprecation")
        SimpleCursorAdapter mAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_spinner_item, objLect.getAnomaliasAll(), from, to);

        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        cmbAnomalias.setAdapter(mAdapter);
    }

    /**
     * Muestra los Datos de la Cuenta
     */
    private void mostrar() {
        if (cRuta != null) {
            int id_cuenta = cRuta.getInt(0);

            cuenta = id_cuenta;

            LecturaDb objLect = new LecturaDb(getApplicationContext());

            // Obtiene los Datos de la Cuenta
            Cursor cRutaCta = objLect.getAll(id_cuenta);

            if (cRutaCta.getCount() > 0)
                cRutaCta.moveToFirst();

            String nombre = cRutaCta.getString(1);
            String direccion = cRutaCta.getString(2);
            String tarifa = cRutaCta.getString(3);
            String situacion = cRutaCta.getString(4);
            String medidor = cRutaCta.getString(5);
            int lect_ant = cRutaCta.getInt(6);
            int id_anomalia = cRutaCta.getInt(7);
            int id_ruta = cRutaCta.getInt(8);
            String clave = cRutaCta.getString(9);
            int sync = cRutaCta.getInt(10);
            int lectura = cRutaCta.getInt(11);
            String observa = cRutaCta.getString(12);

            Spinner cmbAnomalias = (Spinner) findViewById(R.id.cmbAnomalias);
            cmbAnomalias.setSelection(id_anomalia);

            EditText txtCuenta = (EditText) findViewById(R.id.txtCuenta);
            txtCuenta.setText(String.valueOf(id_cuenta));

            EditText txtClave = (EditText) findViewById(R.id.txtClave);
            txtClave.setText(clave);

            EditText txtNombre = (EditText) findViewById(R.id.txtNombre);
            txtNombre.setText(nombre);

            EditText txtDireccion = (EditText) findViewById(R.id.txtDireccion);
            txtDireccion.setText(direccion);

            EditText txtTarifa = (EditText) findViewById(R.id.txtTarifa);
            txtTarifa.setText(tarifa);

            EditText txtSituacion = (EditText) findViewById(R.id.txtSituacion);
            txtSituacion.setText(situacion);

            EditText txtMedidor = (EditText) findViewById(R.id.txtMedidor);
            txtMedidor.setText(medidor);

            EditText txtLectAnt = (EditText) findViewById(R.id.txtLectAnt);
            txtLectAnt.setText(String.valueOf(lect_ant));

            EditText txtIdRuta = (EditText) findViewById(R.id.txtIdRuta);
            txtIdRuta.setText(String.valueOf(id_ruta));

            // Limpiar Datos
            EditText txtLectAct = (EditText) findViewById(R.id.txtLectAct);
            txtLectAct.setText(String.valueOf(lectura));

            EditText txtObserva = (EditText) findViewById(R.id.txtObserva);
            txtObserva.setText(observa);

            BootstrapButton btnEnviar = (BootstrapButton) findViewById(R.id.btnGuardar);

            switch (sync) {
                case 0:
                    btnEnviar.setText("Enviar");
                    btnEnviar.setBootstrapType("success");
                    break;

                case 1:
                    btnEnviar.setText("Corregir");
                    btnEnviar.setBootstrapType("danger");
                    break;

                case 2:
                    btnEnviar.setText("Sincronizar");
                    btnEnviar.setBootstrapType("warning");

            }

            cRutaCta = null;
            btnEnviar = null;
            nombre = null;
            direccion = null;
            tarifa = null;
            situacion = null;
            medidor = null;
            clave = null;
            observa = null;
        }
    }

    /**
     * Lanza layout para cargar ruta
     *
     * @param view View
     */
    public void mnuLoadRuta(View view) {
        Navigator.push(this, R.layout.lyt_key);
    }

    /**
     * Checa llave lectdroid
     *
     * @param view View
     */
    public void checkKey(View view) {
        EditText txtKey = (EditText) findViewById(R.id.txtChkPassword);

        if (Login.checkLectdroidKey(txtKey.getText().toString())) {
            Navigator.pop(this);
            loadViewRuta(view);
        } else
            Toast.makeText(getApplicationContext(), "La llave no es válida ...", Toast.LENGTH_SHORT).show();

        txtKey.setText("");
    }

    /**
     * Lanza View para Cargar Rutas
     *
     * @param view
     */
    private void loadViewRuta(View view) {
        Navigator.push(this, R.layout.lyt_cfgruta);

        if (sectorActual == 0)
            sectorActual = RutaDb.getSector();

        // Sector Actual
        Spinner cmbSector = (Spinner) findViewById(R.id.cmbSector);
        cmbSector.setSelection(sectorActual);
    }


    /**
     * Carga tablas de anomalia y ruta
     *
     * @param view View
     */
    public void loadRuta(View view) {
        Spinner cmbSb = (Spinner) findViewById(R.id.cmbSb);
        Spinner cmbSector = (Spinner) findViewById(R.id.cmbSector);

        AnomaliaDb objAnomalia = new AnomaliaDb(getApplicationContext());
        objAnomalia.deleteAll();
        objAnomalia.fill();

        objAnomalia = null;

        RutaDb objRuta = new RutaDb(getApplicationContext());
        objRuta.deleteAll();
        objRuta.fill(Integer.valueOf(cmbSb.getSelectedItem().toString()).intValue(), Integer.valueOf(cmbSector.getSelectedItem().toString()).intValue());

        objRuta = null;
        cmbSb = null;
        cmbSector = null;

        loadCmbRuta(view);
    }

    /**
     * Carga Combo de Lecturistas de la Ruta
     *
     * @param view View
     */
    public void loadCmbRuta(View view) {
        Spinner cmbSb = (Spinner) findViewById(R.id.cmbSb);
        Spinner cmbSector = (Spinner) findViewById(R.id.cmbSector);

        cmbSb.setEnabled(false);
        cmbSector.setEnabled(false);

        RutaDb objRuta = new RutaDb(getApplicationContext());

        Spinner cmbRutas = (Spinner) findViewById(R.id.cmbRutas);

        String[] from = new String[]{"lecturista"};
        int[] to = new int[]{android.R.id.text1};

        @SuppressWarnings("deprecation")
        SimpleCursorAdapter mAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_spinner_item, objRuta.getAll(), from, to);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        cmbRutas.setAdapter(mAdapter);
        cmbRutas.setEnabled(true);
    }

    /**
     * Carga tabla de Ruta de Lecturas
     *
     * @param view View
     */
    public void loadLecturas(View view) {
        final Spinner cmbRutas = (Spinner) findViewById(R.id.cmbRutas);
        final Spinner cmbSb = (Spinner) findViewById(R.id.cmbSb);
        final Spinner cmbSector = (Spinner) findViewById(R.id.cmbSector);

        final CheckBox chkInactivos = (CheckBox) findViewById(R.id.chkInactivos);
        final CheckBox chkCuotaFija = (CheckBox) findViewById(R.id.chkCuotaFija);
        final CheckBox chkPromedios = (CheckBox) findViewById(R.id.chkPromedios);

        final int inactivos = chkInactivos.isChecked() ? 1 : 0;
        final int cuotaFija = chkCuotaFija.isChecked() ? 1 : 0;
        final int promedios = chkPromedios.isChecked() ? 1 : 0;

        if (cmbRutas.getCount() > 0) {
            // Lanzar Barra
            final ProgressDialog ringProgressDialog = ProgressDialog.show(DapaDroid.this, "Espere por favor ...", "Cargando Ruta de Lecturas ...", true);
            ringProgressDialog.setCancelable(false);

            // Info de Ruta
            RutaDb objRuta = new RutaDb(getApplicationContext());

            SQLiteCursor c = (SQLiteCursor) cmbRutas.getSelectedItem();

            String lecturista = c.getString(1);

            objRuta.setInfoRuta((int) cmbSb.getSelectedItemId() + 1, (int) cmbSector.getSelectedItemId() + 1, lecturista);

            c = null;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        // Obtener Ruta de Lectura
                        LecturaDb objLect = new LecturaDb(getApplicationContext());
                        objLect.deleteAll();
                        objLect.fill((int) cmbSb.getSelectedItemId() + 1, (int) cmbSector.getSelectedItemId() + 1, (int) cmbRutas.getSelectedItemId(), inactivos, cuotaFija, promedios);

                        // Marcar para Sincronizar
                        //objLect.syncAll();

                        currentPos = 0;
                        objLect = null;
                    } catch (Exception e) {
                        Log.e(LOGTAG, "Exception", e);
                    }

                    ringProgressDialog.dismiss();
                }
            }).start();

            Navigator.pop(this);
        } else {
            Toast.makeText(getApplicationContext(), "Debe Seleccionar un Lecturista", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Lanza layout para cargar informacion de la ruta
     *
     * @param view View
     */
    public void mnuLoadInfo(View view) {
        Navigator.push(this, R.layout.lyt_info);

        // Info de Ruta
        TextView infoSb = (TextView) findViewById(R.id.lblInfoSb);
        TextView infoLect = (TextView) findViewById(R.id.lblInfoLect);

        TextView infoTotal = (TextView) findViewById(R.id.lblInfoLecTotal);
        TextView infoEnviadas = (TextView) findViewById(R.id.lblInfoLecEnv);
        TextView infoSync = (TextView) findViewById(R.id.lblInfoLecSync);
        TextView infoRest = (TextView) findViewById(R.id.lblInfoLecRest);

        TextView infoUrl = (TextView) findViewById(R.id.lblInfoUrl);

        RutaDb objRuta = new RutaDb(getApplicationContext());

        String sbSector = objRuta.getInfo();

        if (DapaDroid.offline) {
            infoUrl.setTextAppearance(DapaDroid.this, R.style.bootlbl_danger);
            infoUrl.setText("FUERA DE LINEA");
        }
        else {
            infoUrl.setTextAppearance(DapaDroid.this, R.style.bootlbl_success);
            infoUrl.setText(DapaDroid.base_url);
        }

        if (sbSector.length() == 0) {
            infoSb.setText("NO HAY RUTA CARGADA");
            infoLect.setText("SIN LECTURISTA");

            infoTotal.setText("TOTAL DE LECTURAS: 0");
            infoEnviadas.setText("ENVIADAS: 0");
            infoSync.setText("POR SINCRONIZAR: 0");
            infoRest.setText("POR CAPTURAR: 0");
        } else {
            infoSb.setText(sbSector);
            infoLect.setText(objRuta.getLecturista());

            int total =  objRuta.getInfoTotal();
            int enviadas =  objRuta.getInfoEnviadas();
            int xSync =  objRuta.getPorSync();
            int xCapt = total - (enviadas + xSync);

            infoTotal.setText("TOTAL DE LECTURAS: " + String.valueOf(total));
            infoEnviadas.setText("ENVIADAS: " + String.valueOf(enviadas));
            infoSync.setText("POR SINCRONIZAR: " + String.valueOf(xSync));
            infoRest.setText("POR CAPTURAR: " + String.valueOf(xCapt));

            if (xCapt > 0)
                infoRest.setTextAppearance(DapaDroid.this, R.style.bootlbl_danger);
            else
                infoRest.setTextAppearance(DapaDroid.this, R.style.bootlbl_success);
        }
    }

    /**
     * Lanza layout de Filtro
     *
     * @param view View
     */
    public void mnuFiltro(View view) {
        LecturaDb objLect = new LecturaDb(getApplicationContext());

        // Cargar Ruta
        if (cRuta == null)
            cRuta = objLect.getCuentas();

        Navigator.push(this, R.layout.lyt_filtro);
    }

    /**
     * Si pulsa Aceptar
     *
     * @param view View
     */
    public void filtroOk(View view) {
        medidor = "";

        EditText txtManzana = (EditText) findViewById(R.id.txtFilManzana);

        if (txtManzana.getText().toString().isEmpty())
            manzana = 0;
        else
            manzana = Integer.parseInt(txtManzana.getText().toString());

        mnuCaptureRuta(null);
    }

    /**
     * Si pulsa Aceptar
     *
     * @param view View
     */
    public void filtroOkMed(View view) {
        manzana = 0;

        EditText txtMedidor = (EditText) findViewById(R.id.txtFilMedidor);

        if (! txtMedidor.getText().toString().isEmpty())
            medidor = txtMedidor.getText().toString();

        mnuCaptureRuta(null);
    }

    /**
     * Si pulsa Cancelar
     *
     * @param view View
     */
    public void filtroCancel(View view) {
        manzana = 0;
        medidor = "";

        mnuCaptureRuta(null);
    }

    /**
     * Lanza layout de Ruta de Lectura si existe
     *
     * @param view View
     */
    public void mnuCaptureRuta(View view) {
        try {
            Navigator.push(this, R.layout.lyt_ruta);

            // Cargar Combo
            loadAnomalias();

            boolean encontrado = false;

            if (cRuta.getCount() > 0) {
                // ir a Manzana
                if (manzana > 0) {
                    int totalRows = cRuta.getCount() - 1;

                    for (int i = 0; i <= totalRows; i++) {
                        cRuta.moveToPosition(i);

                        // Primer lote de Manzana
                        if (cRuta.getInt(1) == manzana) {
                            currentPos = i;
                            encontrado = true;
                            break;
                        }
                    }

                    if (! encontrado)
                        Toast.makeText(getApplicationContext(), "La manzana no se encuentra en la Ruta ...", Toast.LENGTH_SHORT).show();
                }

                // Buscar por Medidor
                if (medidor.length() > 0){
                    int totalRows = cRuta.getCount() - 1;

                    for (int i = 0; i <= totalRows; i++) {
                        cRuta.moveToPosition(i);

                        // Primer Medidor
                        if (cRuta.getString(2).trim().equals(medidor.trim())) {
                            currentPos = i;
                            encontrado = true;
                            break;
                        }
                    }

                    if (! encontrado)
                        Toast.makeText(getApplicationContext(), "La medidor no se encuentra en la Ruta ...", Toast.LENGTH_SHORT).show();
                }

                cRuta.moveToPosition(currentPos);
                mostrar();
            } else {
                Toast.makeText(getApplicationContext(), "La ruta no ha sido cargada ...", Toast.LENGTH_SHORT).show();
                Navigator.pop(this);
                //Navigator.push(this, R.layout.lyt_cfg_ruta);
            }
        } catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            Toast.makeText(getApplicationContext(), "Error en Ruta ...", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Mueve al Registro Anterior
     *
     * @param view View
     */
    public void anterior(View view) {
        try {
            if (!cRuta.isFirst()) {
                currentPos--;
                cRuta.moveToPrevious();
                mostrar();
            } else
                currentPos = 0;
        } catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
        }
    }

    /**
     * Mueve al Registro Siguiente
     *
     * @param view View
     */
    public void siguiente(View view) {
        try {
            if (!cRuta.isLast()) {
                cRuta.moveToNext();
                currentPos++;
                mostrar();
            }
        } catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
        }
    }

    /**
     * Enviar Lectura al Sistema Atlantus
     *
     * @param view View
     */
    public void enviar(final View view) {
        EditText txtCuenta = (EditText) findViewById(R.id.txtCuenta);
        EditText txtLectAct = (EditText) findViewById(R.id.txtLectAct);
        EditText txtLectAnt = (EditText) findViewById(R.id.txtLectAnt);
        Spinner cmbAnomalias = (Spinner) findViewById(R.id.cmbAnomalias);
        EditText txtIdRuta = (EditText) findViewById(R.id.txtIdRuta);
        EditText txtObserva = (EditText) findViewById(R.id.txtObserva);
        final int cuenta = Integer.valueOf(txtCuenta.getText().toString()).intValue();

        final LecturaDb objLect = new LecturaDb(getApplicationContext());

        final int lecturaAct = Integer.valueOf(txtLectAct.getText().toString()).intValue();
        int lecturaAnt = Integer.valueOf(txtLectAnt.getText().toString()).intValue();
        final int idAnomalia = (int) cmbAnomalias.getSelectedItemId();
        final int idRuta = Integer.valueOf(txtIdRuta.getText().toString()).intValue();
        final String observa = txtObserva.getText().toString();

        if (lecturaAct > lecturaAnt) {
            int status = objLect.send(cuenta, lecturaAct, idAnomalia, idRuta, observa, location.getLatitude(), location.getLongitude());

            if (status == 3) {
                Toast.makeText(getApplicationContext(), "La lectura no pudo ser enviada, recuerde sincronizar", Toast.LENGTH_SHORT).show();
            }

            txtLectAct.setText("");
            txtObserva.setText("");

            siguiente(view);
        } else {
            AlertDialog.Builder confirmDlg = new AlertDialog.Builder(this);
            confirmDlg.setTitle("DapaDroid");

            if (lecturaAct == 0)
                confirmDlg.setMessage("La lectura es Cero ¿Desea enviar la lectura?");
            else if (lecturaAct == lecturaAnt){
                confirmDlg.setMessage("La lectura es igual a la Lectura Anterior ¿Desea enviar la lectura?");
            }
            else if (lecturaAct < lecturaAnt){
                confirmDlg.setMessage("La lectura es menor a la Lectura Anterior ¿Desea enviar la lectura?");
            }

            confirmDlg.setCancelable(true);
            confirmDlg.setNegativeButton("Cancelar",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            confirmDlg.setPositiveButton(
                    "Aceptar",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            int status = objLect.send(cuenta, lecturaAct, idAnomalia, idRuta, observa, location.getLatitude(), location.getLongitude());

                            if (status == 3) {
                                Toast.makeText(getApplicationContext(), "La lectura no pudo ser enviada, recuerde sincronizar", Toast.LENGTH_SHORT).show();
                            }

                            siguiente(view);
                        }
                    });

            confirmDlg.show();
        }

        txtCuenta = null;
        txtLectAct = null;
        txtLectAnt = null;
        cmbAnomalias = null;
        txtIdRuta = null;
        txtObserva = null;
    }

    /**
     * Lanza layout de Reportes por Cuenta
     *
     * @param view View
     */
    public void mnuLoadReportsCta(View view) {
        if (cRuta != null)
            cuentaRep = cRuta.getInt(0);

        mnuLoadReports(view);
    }

    /**
     * Lanza layout de Reportes
     *
     * @param view View
     */
    public void mnuLoadReports(View view){
        Navigator.push(this, R.layout.lyt_reportes);

        // Si es una cuenta
        if (cuentaRep > 0){
            EditText txtCuenta = (EditText) findViewById(R.id.txtCuentaRep);

            EditText txtManzana = (EditText) findViewById(R.id.txtManzana);
            EditText txtDireccion = (EditText) findViewById(R.id.txtDirRep);

            LecturaDb objLect = new LecturaDb(getApplicationContext());

            Cursor cRutaCta = objLect.getAll(cuentaRep);

            txtCuenta.setText(String.valueOf(cuentaRep));

            if (cRutaCta.getCount() > 0)
                cRutaCta.moveToFirst();

            txtDireccion.setText(cRutaCta.getString(2));
            txtManzana.setText("0");
        }

        // Cargar Combo
        LecturaDb objLect = new LecturaDb(getApplicationContext());
        Spinner cmbAnomalias = (Spinner) findViewById(R.id.cmbAnomalias);

        String[] from = new String[] { "descripcion" };
        int[] to = new int[] { android.R.id.text1 };

        @SuppressWarnings("deprecation")
        SimpleCursorAdapter mAdapter =  new SimpleCursorAdapter(this, android.R.layout.simple_spinner_item, objLect.getAnomaliasAll(), from, to);

        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        cmbAnomalias.setAdapter(mAdapter);

        enableReports(true);
    }

    /**
     * Enable or Disable Widgets of Reports
     * @param yesNo
     */
    public void enableReports(boolean yesNo){
        EditText txtCuenta = (EditText) findViewById(R.id.txtCuentaRep);
        Spinner cmbSb = (Spinner) findViewById(R.id.cmbSbRep);
        Spinner cmbSector = (Spinner) findViewById(R.id.cmbSectorRep);
        EditText txtManzana = (EditText) findViewById(R.id.txtManzana);
        EditText txtDireccion = (EditText) findViewById(R.id.txtDirRep);
        EditText txtObserva = (EditText) findViewById(R.id.txtObservaRep);

        BootstrapButton btnCamera = (BootstrapButton) findViewById(R.id.btnCamera);
        BootstrapButton btnReport = (BootstrapButton) findViewById(R.id.btnReport);

        txtCuenta.setEnabled(yesNo);
        cmbSb.setEnabled(yesNo);
        cmbSector.setEnabled(yesNo);
        txtManzana.setEnabled(yesNo);
        txtDireccion.setEnabled(yesNo);
        txtObserva.setEnabled(yesNo);
        btnReport.setEnabled(yesNo);
        btnCamera.setEnabled(!(yesNo));
    }

    /**
     * Envia Reporte
     *
     * @param view View
     */
    public void sendReport(View view){
        EditText txtCuenta = (EditText) findViewById(R.id.txtCuentaRep);
        Spinner cmbSb = (Spinner) findViewById(R.id.cmbSbRep);
        Spinner cmbSector = (Spinner) findViewById(R.id.cmbSectorRep);
        EditText txtManzana = (EditText) findViewById(R.id.txtManzana);
        EditText txtDireccion = (EditText) findViewById(R.id.txtDirRep);
        Spinner cmbAnomalias = (Spinner) findViewById(R.id.cmbAnomalias);
        EditText txtObserva = (EditText) findViewById(R.id.txtObservaRep);

        int id_cuenta;

        if (txtCuenta.getText().toString().equals(""))
            id_cuenta = 0;
        else
            id_cuenta = Integer.parseInt(txtCuenta.getText().toString());

        int sb = Integer.parseInt(cmbSb.getSelectedItem().toString());
        int sector = Integer.parseInt(cmbSector.getSelectedItem().toString());

        int manzana;
        if (txtManzana.getText().toString().equals(""))
            manzana = 0;
        else
            manzana = Integer.parseInt(txtManzana.getText().toString());

        String direccion = txtDireccion.getText().toString();
        int idAnomalia =  (int)cmbAnomalias.getSelectedItemId();
        String observa = txtObserva.getText().toString();

        // Obtener Id Anomalia
        AnomaliaDb objAnomalia = new AnomaliaDb(this.getApplicationContext());
        idAnomalia = objAnomalia.getIdAnomalia(idAnomalia);
        objAnomalia = null;

        ReportesDb objReporte = new ReportesDb(getApplicationContext());
        folio = objReporte.send(id_cuenta, sb, sector, manzana, direccion, idAnomalia, observa, location.getLatitude(), location.getLongitude(), false);

        if (folio > 0) {
            Toast.makeText(getApplicationContext(), "Folio Generado: " + String.valueOf(folio), Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getApplicationContext(), "El reporte no pudo ser enviado, debe sincronizar posteriormente", Toast.LENGTH_SHORT).show();
        }

        enableReports(false);

        objReporte = null;
    }

    /**
     * Lanza Activity Camara
     *
     * @param view
     */
    public void lectCam(View view){
        //LectCam
        new Thread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(DapaDroid.this, LectCam.class);
                intent.putExtra("folio", folio);
                startActivity(intent);
            }
        }).start();
    }

    /**
     * Lanza Activity Camara
     *
     * @param view
     */
    public void lectCamCta(View view){
        //LectCam
        new Thread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(DapaDroid.this, LectCam.class);
                intent.putExtra("cuenta", cuenta);
                startActivity(intent);
            }
        }).start();
    }

    /**
     * Sincroniza Fotos
     */
    private void syncPictures() {
        // Photos
        new Thread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(DapaDroid.this, LectCam.class);
                intent.putExtra("sync", 1);
                startActivity(intent);
            }
        }).start();
    }

    /**
     * Sincroniza Lecturas
     */
    private void syncLecturas() {
        // Lecturas
        final LecturaDb objLect = new LecturaDb(getApplicationContext());

        final Cursor cSync = objLect.getRowsSync();

        if (cSync.getCount() > 0) {
            AlertDialog.Builder confirmDlg = new AlertDialog.Builder(this);
            confirmDlg.setTitle("DapaDroid");
            confirmDlg.setMessage("¿Desea sincronizar " + String.valueOf(cSync.getCount()) + " lecturas?");
            confirmDlg.setCancelable(true);
            confirmDlg.setNegativeButton("Cancelar",  null);
            confirmDlg.setPositiveButton(
                    "Aceptar",
                    new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int id) {

                            final ProgressDialog ringProgressDialog = ProgressDialog.show(DapaDroid.this, "Espere por favor ...", "Sincronizando Lecturas ...", true);
                            ringProgressDialog.setCancelable(false);

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        cSync.moveToFirst();

                                        do {
                                            int cuenta = cSync.getInt(0);
                                            int lectura = cSync.getInt(1);
                                            int idAnomalia = cSync.getInt(2);
                                            int idRuta = cSync.getInt(3);
                                            String observa = cSync.getString(4);

                                            // Sincronizar Lectura
                                            objLect.send(cuenta, lectura , idAnomalia, idRuta, observa, location.getLatitude(), location.getLongitude());
                                        } while(cSync.moveToNext());

                                    } catch (Exception e) {
                                        Log.e(LOGTAG, "Exception", e);
                                    }

                                    ringProgressDialog.dismiss();
                                }
                            }).start();
                        }
                    });

            confirmDlg.show();
        }
        else
            Toast.makeText(getApplicationContext(), "No existen lecturas para Sincronizar", Toast.LENGTH_SHORT).show();
    }

    /**
     * Sincronizar Reportes
     */
    private void syncReports () {
        // Lecturas
        final ReportesDb objReports = new ReportesDb(getApplicationContext());

        final Cursor cSync = objReports.getRowsSync();

        if (cSync.getCount() > 0) {
            AlertDialog.Builder confirmDlg = new AlertDialog.Builder(this);
            confirmDlg.setTitle("DapaDroid");
            confirmDlg.setMessage("¿Desea sincronizar " + String.valueOf(cSync.getCount()) + " reportes?");
            confirmDlg.setCancelable(true);
            confirmDlg.setNegativeButton("Cancelar",  null);
            confirmDlg.setPositiveButton(
                    "Aceptar",
                    new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int id) {

                            final ProgressDialog ringProgressDialog = ProgressDialog.show(DapaDroid.this, "Espere por favor ...", "Sincronizando reportes ...", true);
                            ringProgressDialog.setCancelable(false);

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        cSync.moveToFirst();

                                        do {
                                            int idReporte = cSync.getInt(0);
                                            int idCuenta = cSync.getInt(1);
                                            int sb = cSync.getInt(2);
                                            int sector = cSync.getInt(3);
                                            int manzana = cSync.getInt(4);
                                            String direcc = cSync.getString(5);
                                            int idAnomalia = cSync.getInt(6);
                                            String observa = cSync.getString(7);

                                            // Sincronizar Reporte
                                            int syncFolio = objReports.send(idCuenta, sb, sector, manzana, direcc, idAnomalia, observa, location.getLatitude(), location.getLongitude(), true);

                                            // Actualizar Fotos de Reporte Sincronizado
                                            if (syncFolio > 0)
                                                objReports.updatePicSync(idReporte, syncFolio);

                                        } while(cSync.moveToNext());

                                    } catch (Exception e) {
                                        Log.e(LOGTAG, "Exception", e);
                                    }

                                    ringProgressDialog.dismiss();
                                }
                            }).start();
                        }
                    });

            confirmDlg.show();
        }
        else
            Toast.makeText(getApplicationContext(), "No existen Reportes para Sincronizar", Toast.LENGTH_SHORT).show();
    }

    /**
     * Sincroniza los registros que no se pudieron enviar
     *
     * @param view View
     */
    public void mnuSincronizar(View view){
        // Sincronizar Lecturas
        syncLecturas();

        // Sincronizar Reportes
        syncReports();

        // Sincronizar Fotos
        syncPictures();
    }

    /**
     * Lanza layout de Creditos
     *
     * @param view View
     */
    public void mnuLoadCredits(View view){
        Navigator.push(this, R.layout.lyt_credits);
    }

    /**
     * Open URL in Browser
     *
     * @param url
     */
    private void loadUrl(final String url){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
                catch (ActivityNotFoundException e){
                    Toast.makeText(getApplicationContext(), "No se puede abrir Activity", Toast.LENGTH_SHORT).show();
                }
            }
        }).start();
    }

    /**
     * Lanza URL GitHub
     *
     * @param view View
     */
    public void loadGithub(View view){
        this.loadUrl("https://github.com/yorch81");
    }

    /**
     * Lanza URL BitBucket
     *
     * @param view View
     */
    public void loadBitbucket(View view){
        this.loadUrl("https://bitbucket.org/yorch81");
    }

    /**
     * Lanza URL Blog
     *
     * @param view View
     */
    public void loadBlog(View view){
        this.loadUrl("http://the-yorch.blogspot.mx/");
    }

    /**
     * Lanza URL Email
     *
     * @param view View
     */
    public void loadMail(View view){
        this.loadUrl("mailto:the.yorch@gmail.com?subject=Contacto");
    }
}
