package net.yorch.dapadroid;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Splash<br>
 *
 * Splash carga el activity splash de la aplicacion<br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version 1.0.0, 2015-09-08
 * @author <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class Splash extends AppCompatActivity {
    /**
     * Retardo del Splash
     */
    private static final long SPLASH_SCREEN_DELAY = 2000;

    /**
     * Evento onCreate
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Hide title bar
        //requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.lyt_splash);

        /*
        ImageView logo;
        Animation rotar;
        logo = (ImageView) findViewById(R.id.spl_logo);
        rotar = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotar);
        logo.setAnimation(rotar);
        */

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                Intent mainIntent = new Intent().setClass(Splash.this, DapaDroid.class);
                startActivity(mainIntent);

                finish();
            }
        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }

    /**
     * Back Button Event
     *
     * @param keyCode
     * @param event
     * @return
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            return false;
        }

        return super.onKeyDown(keyCode, event);
    }
}
