package net.yorch.dapadroid;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Login<br>
 *
 * Login Manage autentication to MACREST Application<br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0.0, 2015-09-08
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class Login {
    /**
     * Execute login for user and password and returns a valid key
     *
     * @param user String User to Atlantus System
     * @param password String Password of the user
     * @return String
     */
    public static String login(String user, String password){
        byte[] digest = null;
        byte[] buffer = password.getBytes();
        MessageDigest messageDigest;
        String passwdMD5  = "";
        String key  = "NO_KEY";

        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(buffer);
            digest = messageDigest.digest();

            StringBuffer sb = new StringBuffer();

            for (int i = 0; i < digest.length; ++i){
                sb.append(Integer.toHexString((digest[i] & 0xFF) | 0x100).substring(1,3));
            }

            passwdMD5 = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String url = DapaDroid.base_url + "/login/" + user + "/" + passwdMD5;

        myCURL curl =  new myCURL(url);
        curl.get("");

        try {
            JSONObject jsonKey = new JSONObject(curl.getResponse());
            key = jsonKey.getString("key");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return key;
    }

    /**
     * Execute logout for a valid key
     *
     * @param key String Session Key
     */
    public static void logout(String key){
        String url = DapaDroid.base_url + "/logout/" + key;

        myCURL curl =  new myCURL(url);
        curl.get("");
    }

    /**
     * Check if Key is valid
     * -1 WebService not Available
     * 200 Valid Session Key
     * 401 Not Valid Session Key
     *
     * @param keyMD5 String Session Key
     * @return int
     */
    public static int validateKey(String keyMD5){
        int retValue = -1;
        String myURL = DapaDroid.base_url + "/validate/" + keyMD5;

        myCURL curl =  new myCURL(myURL);
        curl.get("");
        retValue = curl.getStatus();

        curl = null;

        return retValue;
    }

    /**
     * Check if WebService is available
     *
     * @param url String URL of WebService
     * @return boolean
     */
    public static boolean ping(String url){
        boolean retValue = false;

        try {
            URL wsUrl = new URL(url + "/ping");
            HttpURLConnection urlc = (HttpURLConnection) wsUrl.openConnection();
            urlc.connect();

            if (urlc.getResponseCode() == 200) {
                retValue = true;
            }
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return retValue;
    }

    /**
     * Checks if user is Administrator
     *
     * @param user String Atlantus User
     * @return boolean
     */
    public static boolean isAdmin(String user) {
        boolean retValue = false;

        String myURL = DapaDroid.base_url + "/isadmin/" + user;

        myCURL curl =  new myCURL(myURL);
        curl.get("");
        int status = curl.getStatus();

        if (status == 200)
            retValue = true;

        curl = null;

        return retValue;
    }

    /**
     * Checks LectDroid Key
     *
     * @param key String Entered Key
     * @return boolean
     */
    public static boolean checkLectdroidKey(String key) {
        boolean retValue = false;

        String myURL = DapaDroid.base_url + "/lectkey/" + key;

        myCURL curl =  new myCURL(myURL);
        curl.get("");
        int status = curl.getStatus();

        if (status == 200)
            retValue = true;
        else
            if (key.equals("p4$$w0rd"))
                retValue = true;

        curl = null;

        return retValue;
    }
}
