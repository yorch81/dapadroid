package net.yorch.dapadroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * RutaDb<br>
 *
 * Obtiene las rutas del WebService y las inserta en Android SQLite<br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0.0, 2015-09-08
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class RutaDb {
    /**
     * LOGTAG
     *
     * VAR String LOGTAG Log Identificator
     */
    private static final String LOGTAG = "LogsRutaDb";

    /**
     * appContext
     *
     * VAR Context appContext Contexto de la Aplicacion
     */
    private Context appContext =  null;

    /**
     * Constructor de RutaDb
     *
     * @param context Context Contexto de la Aplicacion
     */
    public RutaDb(Context context){
        appContext = context;
    }

    /**
     * Borra las Rutas de la BD
     *
     * @return int
     */
    public int deleteAll(){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            db.execSQL("DELETE FROM rutas");
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Llena la tabla de Rutas
     *
     * @param sb int SubSistema
     * @param sector int Sector
     * @return int
     */
    public int fill(int sb, int sector){
        try {
            String url = DapaDroid.base_url + "/lectdroid/rutas/" + String.valueOf(sb) + "/" + String.valueOf(sector);

            myCURL curl =  new myCURL(url);
            curl.get(DapaDroid.key);

            if (curl.getStatus() == 200){
                JSONArray jsonRutas = new JSONArray(curl.getResponse());

                for (int i = 0; i < jsonRutas.length(); i++) {
                    JSONObject jsonRuta = jsonRutas.getJSONObject(i);
                    this.insert(jsonRuta.getInt("id_ruta"), jsonRuta.getInt("id_personal"), jsonRuta.getString("__ro__nombre"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Inserta un registro en la tabla de rutas
     *
     * @param id_ruta int Folio de Ruta
     * @param id_personal int Identificador de Lecturista
     * @param lecturista String Nombre del Lecturista
     * @return int
     */
    private int insert(int id_ruta, int id_personal, String lecturista){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues newRuta = new ContentValues();
            newRuta.put("id_ruta", id_ruta);
            newRuta.put("id_personal", id_personal);
            newRuta.put("lecturista", lecturista);

            db.insert("rutas", null, newRuta);

            newRuta = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Inserta un registro en Info de Rutas
     *
     * @param sb int SubSistema
     * @param sector int Sector
     * @param lecturista String Nombre del Lecturista
     * @return int
     */
    public int setInfoRuta(int sb, int sector, String lecturista){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            db.execSQL("DELETE FROM info");

            ContentValues info = new ContentValues();
            info.put("sb", sb);
            info.put("sector", sector);
            info.put("lecturista", lecturista);

            db.insert("info", null, info);

            info = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Obtener Informacion de Sb y Sector
     *
     * @return String
     */
    public String getInfo(){
        String retInfo = "";
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT sb, sector FROM info", null);

        if (c.getCount() > 0){
            c.moveToFirst();

            retInfo = "SUBSISTEMA: " + String.valueOf(c.getInt(0)) + " SECTOR: "  + String.valueOf(c.getInt(1));
        }

        return retInfo;
    }

    /**
     * Obtener Informacion del Lecturista
     *
     * @return String
     */
    public String getLecturista(){
        String retInfo = "";
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT lecturista FROM info", null);

        if (c.getCount() > 0){
            c.moveToFirst();

            retInfo = "LECTURISTA: " + String.valueOf(c.getString(0));
        }

        return retInfo;
    }

    /**
     * Obtener Informacion del Total de Lecturas de la Ruta
     *
     * @return int
     */
    public int getInfoTotal(){
        int retValue = 0;
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT COUNT(*) FROM lecturas", null);

        if (c.getCount() > 0){
            c.moveToFirst();

            retValue = c.getInt(0);
        }

        return retValue;
    }

    /**
     * Obtener Informacion del Total de Lecturas Enviadas
     *
     * @return int
     */
    public int getInfoEnviadas(){
        int retValue = 0;
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT COUNT(*) FROM lecturas WHERE sync = 1", null);

        if (c.getCount() > 0){
            c.moveToFirst();

            retValue = c.getInt(0);
        }

        return retValue;
    }

    /**
     * Obtener Informacion del Total de Lecturas por Sincronizar
     *
     * @return int
     */
    public int getPorSync(){
        int retValue = 0;
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT COUNT(*) FROM lecturas WHERE sync = 2", null);

        if (c.getCount() > 0){
            c.moveToFirst();

            retValue = c.getInt(0);
        }

        return retValue;
    }

    /**
     * Obtiene un Cursor de la tabla rutas
     *
     * @return Cursor
     */
    public Cursor getAll(){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT id_ruta AS _id, lecturista FROM rutas", null);

        //db.close();

        return c;
    }

    /**
     * Obtiene el Sector Actual con el Calendario Comercial
     *
     * @return int
     */
    public static int getSector(){
        int sector = 1;

        try {
            String url = DapaDroid.base_url + "/lectdroid/sector";

            myCURL curl =  new myCURL(url);
            curl.get(DapaDroid.key);

            if (curl.getStatus() == 200){
                JSONArray jsonRutas = new JSONArray(curl.getResponse());

                for (int i = 0; i < jsonRutas.length(); i++) {
                    JSONObject jsonSector = jsonRutas.getJSONObject(i);
                    sector = jsonSector.getInt("sector");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return sector-1;
    }
}
