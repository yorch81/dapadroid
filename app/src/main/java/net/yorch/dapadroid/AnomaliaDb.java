package net.yorch.dapadroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * AnomaliaDb<br>
 *
 * Obtiene las anomalias del WebService y las inserta en la BD SQLite<br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0.0, 2015-09-08
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class AnomaliaDb {
    /**
     * LOGTAG
     *
     * VAR String LOGTAG Log Identificator
     */
    private static final String LOGTAG = "LogsAnomaliaDb";

    /**
     * appContext
     *
     * VAR Context appContext Contexto de la Aplicacion
     */
    private Context appContext =  null;

    /**
     * Constructor de AnomaliaDb
     *
     * @param context Context Contexto de la Aplicacion
     */
    public AnomaliaDb(Context context){
        appContext = context;
    }

    /**
     * Borra las Anomalias de la BD
     *
     * @return int
     */
    public int deleteAll(){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            db.execSQL("DELETE FROM anomalias");
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Llena la tabla de anomalias
     *
     * @return int
     */
    public int fill(){
        int retValue = 0;

        String url = DapaDroid.base_url + "/lectdroid/anomalias";

        myCURL curl =  new myCURL(url);
        curl.get(DapaDroid.key);

        if (curl.getStatus() == 200){
            try {
                JSONArray jsonAnomalias = new JSONArray(curl.getResponse());

                for (int i = 0; i < jsonAnomalias.length(); i++) {
                    JSONObject jsonAnom = jsonAnomalias.getJSONObject(i);
                    this.insert(i, jsonAnom.getInt("id_anomalia"), jsonAnom.getString("descripcion"));
                }

                retValue =  0;
            } catch (JSONException e) {
                e.printStackTrace();
                retValue = 1;
            }
        }

        return retValue;
    }

    /**
     * Inserta un registro en la tabla de anomalias
     *
     * @param id int Identificador de Posicion
     * @param id_anomalia int Identificador de Anomalia
     * @param descripcion String Descripcion de Anomalia
     * @return int
     */
    private int insert(int id, int id_anomalia, String descripcion){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues newAnomalia = new ContentValues();
            newAnomalia.put("id", id);
            newAnomalia.put("id_anomalia", id_anomalia);
            newAnomalia.put("descripcion",descripcion);

            db.insert("anomalias", null, newAnomalia);

            newAnomalia = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Obtiene el id_anomalia seleccionado
     *
     * @param id Id Posicion
     * @return int
     */
    public int getIdAnomalia(int id){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        try{
            Cursor c = db.rawQuery("SELECT id_anomalia FROM anomalias WHERE id = " + String.valueOf(id), null);

            if (c.getCount() > 0){
                c.moveToFirst();
                retValue = c.getInt(0);
            }
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 0;
        }

        db.close();

        return retValue;
    }
}
