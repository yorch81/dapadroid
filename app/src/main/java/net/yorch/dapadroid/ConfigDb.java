package net.yorch.dapadroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * ConfigDb<br>
 *
 * Persiste en SQLite la URL y KEY para la aplicacion<br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0.0, 2015-08-09
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class ConfigDb {
    /**
     * LOGTAG
     *
     * VAR String LOGTAG Log Identificator
     */
    private static final String LOGTAG = "LogsConfigDb";

    /**
     * appContext
     *
     * VAR Context appContext Log Contexto de la Aplicacion
     */
    private Context appContext =  null;

    /**
     * Constructor of ConfigDb
     *
     * @param context Context Contexto de la Aplicacion
     */
    public ConfigDb(Context context) {
        appContext = context;
    }

    /**
     * Actualiza url y key en la Base de Datos
     *
     * @param url String URL a guardar
     * @param keyMD5 String Llave de Sesion
     * @param current int Posicion Actual
     * @param user String Usuario de la App
     * @return int
     */
    public int update(String url, String keyMD5, int current, String user){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues updConfig = new ContentValues();
            updConfig.put("url", url);
            updConfig.put("key", keyMD5);
            updConfig.put("current", current);
            updConfig.put("user", user);

            db.update("config", updConfig, "id=0", null);

            updConfig = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Set Current Position
     *
     * @param current int Current Position
     * @return boolean
     */
    public boolean setCurrent(int current){
        boolean retValue = true;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues updConfig = new ContentValues();
            updConfig.put("current", current);

            db.update("config", updConfig, "id=0", null);

            updConfig = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = false;
        }

        db.close();

        return retValue;
    }

    /**
     * Update URL
     *
     * @param url String URL
     * @return boolean
     */
    public boolean setURL(String url){
        boolean retValue = true;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues updConfig = new ContentValues();
            updConfig.put("url", url);

            db.update("config", updConfig, "id=0", null);

            updConfig = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = false;
        }

        db.close();

        return retValue;
    }

    /**
     * Obtiene la URL configurada
     *
     * @return String
     */
    @SuppressWarnings("deprecation")
    public String getURL(){
        String retValue = "";

        DapaDb objDB = new DapaDb(appContext);
        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT url FROM config WHERE id = 0", null);

        c.moveToFirst();
        retValue = c.getString(0);

        c.deactivate();
        c.close();
        db.close();

        return retValue;
    }

    /**
     * Obtiene la Llave de Sesion configurada
     *
     * @return String
     */
    @SuppressWarnings("deprecation")
    public String getKey(){
        String retValue = "";

        DapaDb objDB = new DapaDb(appContext);
        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT key FROM config WHERE id = 0", null);

        c.moveToFirst();
        retValue = c.getString(0);

        c.deactivate();
        c.close();
        db.close();

        return retValue;
    }

    /**
     * Obtiene Posicion Actual
     *
     */
    @SuppressWarnings("deprecation")
    public int getCurrent(){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);
        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT current FROM config WHERE id = 0", null);

        c.moveToFirst();
        retValue = c.getInt(0);

        c.deactivate();
        c.close();
        db.close();

        return retValue;
    }

    /**
     * Obtiene usuario logeado
     *
     * @return String
     */
    @SuppressWarnings("deprecation")
    public String getUser() {
        String retValue = "";

        DapaDb objDB = new DapaDb(appContext);
        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT user FROM config WHERE id = 0", null);

        c.moveToFirst();
        retValue = c.getString(0);

        c.deactivate();
        c.close();
        db.close();

        return retValue;
    }
}
