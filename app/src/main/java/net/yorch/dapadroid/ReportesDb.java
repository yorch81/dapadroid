package net.yorch.dapadroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

/**
 * ReportesDb<br>
 *
 * Envia reportes a MACREST o persiste el registro<br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0.0, 2015-08-09
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class ReportesDb {
    /**
     * LOGTAG
     *
     * VAR String LOGTAG Log Identificator
     */
    private static final String LOGTAG = "LogsReporteDb";

    /**
     * appContext
     *
     * VAR Context appContext Contexto de la Aplicacion
     */
    private Context appContext =  null;

    /**
     * Constructor de AnomaliaDb
     *
     * @param context Context Contexto de la Aplicacion
     */
    public ReportesDb(Context context){
        appContext = context;
    }

    /**
     * Enviar Reporte
     *
     * @param id_cuenta int Cuenta de usuario
     * @param sb 		int Subsistema
     * @param sector    int Sector
     * @param manzana   int Manzana
     * @param direccion String Direccion
     * @param id_anomalia int Id de Anomalia
     * @param observa   String Observaciones
     * @param latitud   double Latitud
     * @param longitud  double Longitud
     * @param isSync boolan Indica si está sincronizando
     * @return int
     */
    public int send(int id_cuenta, int sb, int sector, int manzana, String direccion, int id_anomalia, String observa, double latitud, double longitud, boolean isSync){
        int retValue = 0;

        try {
            JSONObject json = new JSONObject();
            json.put("IdCuenta", id_cuenta);
            json.put("sb", sb);
            json.put("sector", sector);
            json.put("manzana", manzana);
            json.put("direccion", direccion);
            json.put("IdAnomalia", id_anomalia);
            json.put("observa", observa);
            json.put("Latitud", latitud);
            json.put("Longitud", longitud);

            String urlPost = DapaDroid.base_url + "/lectdroid/reportes";
            myCURL curl =  new myCURL(urlPost);
            curl.setPostData("ANDROID", json.toString());

            curl.post(DapaDroid.key);

            if (curl.getStatus() == 201){
                JSONArray jsonResponse = new JSONArray(curl.getResponse());
                JSONObject jsonId = jsonResponse.getJSONObject(0);

                retValue = jsonId.getInt("id");
            }
            else{
                if (! isSync)
                    retValue = insert(id_cuenta, sb, sector, manzana, direccion, id_anomalia, observa);
            }
        } catch (JSONException e) {
            retValue = 0;
            e.printStackTrace();
        }

        return retValue;
    }

    /**
     * Guarda la ruta de la Foto y el reporte al que pertenece
     *
     * @param id_reporte int Folio de Reporte
     * @param picture    String Ruta de Foto
     * @return int
     */
    public int savePic(int id_reporte, String picture){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues insPicture= new ContentValues();

            insPicture.put("id_reporte", id_reporte);
            insPicture.put("picture", picture);
            insPicture.put("sync", 0);

            db.insert("pictures", null, insPicture);

            insPicture = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Actualiza las Fotos de un reporte sincronizado
     *
     * @param id int Id Temporal
     * @param id_reporte int Id Reporte Sincronizado
     */
    public void updatePicSync(int id, int id_reporte){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            // Actualizar Pictures
            String sql = "update pictures set id_reporte=" + String.valueOf(id_reporte) + " where id_reporte=" + String.valueOf(id);
            db.execSQL(sql);

            // Actualizar Reportes
            sql = "update reportes set id=" + String.valueOf(id_reporte) + " where id=" + String.valueOf(id);
            db.execSQL(sql);
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
        }

        db.close();
    }

    /**
     * Guarda la ruta de la Foto y la cuenta a la que pertenece
     *
     * @param id_cuenta  int    Cuenta de usuario
     * @param picture    String Ruta de Foto
     * @return int
     */
    public int savePicCta(int id_cuenta, String picture){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues insPicture= new ContentValues();

            insPicture.put("id_cuenta", id_cuenta);
            insPicture.put("picture", picture);
            insPicture.put("sync", 0);

            db.insert("pictures_cta", null, insPicture);

            insPicture = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Determina si es necesario sincronizar lecturas y reportes antes que las Fotos
     *
     * @return boolean
     */
    public boolean reportesXSync(){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        boolean retValue = false;

        Cursor c = db.rawQuery("SELECT COUNT(*) AS TOTAL FROM pictures WHERE id_reporte < 0", null);

        c.moveToFirst();

        if (c.getInt(0) > 0)
            retValue = true;

        db.close();

        return retValue;
    }

    /**
     *  Get Pictures to Sincronize
     *
     * @return Cursor
     */
    public Cursor getPicSync(){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT id, id_reporte, picture FROM pictures WHERE sync = 0 AND id_reporte > 0", null);

        //db.close();

        return c;
    }

    /**
     *  Get Pictures to Sincronize
     *
     * @return Cursor
     */
    public Cursor getPicSyncCta(){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT id, id_cuenta, picture FROM pictures_cta WHERE sync = 0", null);

        //db.close();

        return c;
    }

    /**
     * Update Sync Picture
     *
     * @param id_picture Picture Id
     * @return int
     */
    public int updatePic(int id_picture){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues updPictures = new ContentValues();

            updPictures.put("sync", 1);

            db.update("pictures", updPictures, "id=" + String.valueOf(id_picture), null);

            updPictures = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Update Sync Picture Cta
     *
     * @param id_picture Picture Id
     * @return int
     */
    public int updatePicCta(int id_picture){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues updPictures = new ContentValues();

            updPictures.put("sync", 1);

            db.update("pictures_cta", updPictures, "id=" + String.valueOf(id_picture), null);

            updPictures = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Inserta Reporte en SQLite
     *
     * @param id_cuenta int Cuenta de usuario
     * @param sb        int Subsistema
     * @param sector    int Sector
     * @param manzana   int Manzana
     * @param direccion String Direccion
     * @param id_anomalia int Folio de Anomalia
     * @param observa String Observaciones
     * @return int
     */
    private int insert(int id_cuenta, int sb, int sector, int manzana, String direccion, int id_anomalia, String observa){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues insReporte = new ContentValues();

            int id = getNegativeId();

            insReporte.put("id", id);
            insReporte.put("id_cuenta", id_cuenta);
            insReporte.put("sb", sb);
            insReporte.put("sector", sector);
            insReporte.put("manzana", manzana);
            insReporte.put("direc", direccion);
            insReporte.put("id_anomalia", id_anomalia);
            insReporte.put("observa", observa);

            db.insert("reportes", null, insReporte);

            insReporte = null;

            retValue = id;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Obtiene Id Negativo para Reportes
     */
    private int getNegativeId(){
        Random r = new Random();
        int id = r.nextInt();

        if (id > 0)
            id = id * -1;

        return id;
    }

    /**
     * Obtiene los Reportes a Sincronizar
     *
     * @return Cursor
     */
    public Cursor getRowsSync(){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT id, id_cuenta, sb, sector, manzana, direc, id_anomalia, observa FROM reportes WHERE id < 0", null);

        //db.close();

        return c;
    }
}
