package net.yorch.dapadroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * LecturaDb<br>
 *
 * Obtiene la ruta de lectura del WebService y las inserta en Android SQLite
 * inserta la nueva lectura en Atlantus mediante el WebService y en caso que
 * no tenga conexion a internet guarda la lectura localmente para despues
 * sincronizar.<br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0.0, 2015-09-08
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class LecturaDb {
    /**
     * LOGTAG
     *
     * VAR String LOGTAG Log Identificator
     */
    private static final String LOGTAG = "LogsLecturaDb";

    /**
     * appContext
     *
     * VAR Context appContext Contexto de la Aplicacion
     */
    private Context appContext =  null;

    /**
     * Constructor de LecturaDb
     *
     * @param context Context Contexto de la Aplicacion
     */
    public LecturaDb(Context context){
        appContext = context;
    }

    /**
     * Borra las Lecturas de la BD
     *
     * @return int
     */
    public int deleteAll(){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            db.execSQL("DELETE FROM lecturas");
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Llena la tabla de Lecturas
     *
     * @param sb int Subsistema
     * @param sector int Sector
     * @param ruta int Identificador de Ruta
     * @param inactivos int 0 - 1
     * @param cuotaFija int 0 - 1
     * @param promedios int 0 - 1
     * @return int
     */
    public int fill(int sb, int sector, int ruta, int inactivos, int cuotaFija, int promedios){
        try {
            String url = DapaDroid.base_url + "/lectdroid/lecturas/" + String.valueOf(sb) + "/" + String.valueOf(sector) + "/"
                    + String.valueOf(ruta) + "/" + String.valueOf(inactivos) + "/" + String.valueOf(cuotaFija) + "/" + String.valueOf(promedios);

            myCURL curl =  new myCURL(url);
            curl.get(DapaDroid.key);

            if (curl.getStatus() == 200){
                JSONArray jsonLecturas = new JSONArray(curl.getResponse());

                for (int i = 0; i < jsonLecturas.length(); i++) {
                    JSONObject jsonLect = jsonLecturas.getJSONObject(i);

                    this.insert(jsonLect.getInt("id_cuenta"),
                            jsonLect.getString("nombre"),
                            jsonLect.getString("direccion"),
                            jsonLect.getString("tarifa"),
                            jsonLect.getString("situacion"),
                            jsonLect.getString("medidor"),
                            jsonLect.getInt("lec_anterior"),
                            0, //jsonLect.getInt("anomalia")
                            jsonLect.getInt("ruta"),
                            jsonLect.getString("cve_loc"),
                            jsonLect.getInt("manzana"));
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Inserta un registro en la tabla de Lecturas
     *
     * @param id_cuenta int Cuenta de Usuario
     * @param nombre String Nombre de Usuario
     * @param direccion String Direccion de Usuario
     * @param tarifa String Tarifa de Usuario
     * @param situacion String Situacion de Toma de Usuario
     * @param medidor String Folio de Medidor
     * @param lect_ant int Lectura Anterior
     * @param id_anomalia int Anomaloa
     * @param id_ruta int Identificador de Ruta
     * @param cve_loc String Clave de Localizacion
     * @param manzana int Manzana
     * @return int
     */
    private int insert(int id_cuenta, String nombre, String direccion, String tarifa, String situacion, String medidor, int lect_ant, int id_anomalia, int id_ruta, String cve_loc, int manzana){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues newLectura = new ContentValues();
            newLectura.put("id_cuenta", id_cuenta);
            newLectura.put("nombre", nombre);
            newLectura.put("direccion", direccion);
            newLectura.put("tarifa", tarifa);
            newLectura.put("situacion", situacion);
            newLectura.put("medidor", medidor);
            newLectura.put("lect_ant", lect_ant);
            newLectura.put("id_anomalia", id_anomalia);
            newLectura.put("id_ruta", id_ruta);
            newLectura.put("cve_loc", cve_loc);
            newLectura.put("lectura", 0);
            newLectura.put("observa", "");
            newLectura.put("sync", 0);
            newLectura.put("manzana", manzana);

            db.insert("lecturas", null, newLectura);

            newLectura = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Actualiza registro si se pudo enviar
     *
     * @param id_cuenta int Cuenta de Usario
     * @param lectura int Nueva lectura
     * @param id_anomalia int Identificador de Anomalia
     * @param observa String Observaciones de la Lectura
     * @param sync int Sincronizado 1 = Enviado o 2 = Por sincronizar
     * @return int
     */
    private int update(int id_cuenta, int lectura, int id_anomalia, String observa, int sync){
        int retValue = 0;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            ContentValues updLectura = new ContentValues();

            updLectura.put("id_anomalia", id_anomalia);
            updLectura.put("lectura", lectura);
            updLectura.put("observa", observa);
            updLectura.put("sync", sync);

            db.update("lecturas", updLectura, "id_cuenta=" + String.valueOf(id_cuenta), null);

            updLectura = null;
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
            retValue = 1;
        }

        db.close();

        return retValue;
    }

    /**
     * Marcar lecturas como sincronizables
     */
    public void syncAll(){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            db.execSQL("update lecturas set sync = 2");
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
        }

        db.close();
    }

    /**
     * Finaliza la Ruta
     * Cambio: Anomalia 12
     */
    public void finalizar(){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getWritableDatabase();

        try{
            db.execSQL("update lecturas set sync=2, lectura=0, id_anomalia=12, observa='DAPADROID: no pudo capturar la lectura'  where sync=0");
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Exception", e);
        }

        db.close();
    }

    /**
     * Envia la nueva lectura mediante MACREST al Sistema Atlantus
     *
     * @param IdCuenta int Cuenta de Usario
     * @param Lectura int Nueva lectura
     * @param IdAnomalia int Identificador de Anomalia
     * @param IdRuta int Identificador de Ruta
     * @param Observa String Observaciones
     * @param latitud double Latitud
     * @param longitud double Longitud
     * @return int
     */
    public int send(int IdCuenta, int Lectura, int IdAnomalia, int IdRuta, String Observa, double latitud, double longitud){
        int retValue = 0;

        try {
            AnomaliaDb objAnomalia = new AnomaliaDb(appContext);

            JSONObject json = new JSONObject();
            json.put("IdCuenta", IdCuenta);
            json.put("Lectura", Lectura);
            json.put("IdAnomalia", objAnomalia.getIdAnomalia(IdAnomalia));
            json.put("IdRuta", IdRuta);
            json.put("Observa", Observa);
            json.put("Latitud", latitud);
            json.put("Longitud", longitud);

            String urlPost = DapaDroid.base_url + "/lectdroid/actlectura";
            myCURL curl =  new myCURL(urlPost);
            curl.setPostData("ANDROID", json.toString());
            curl.post(DapaDroid.key);

            // Si ya tiene una Lectura
            if (curl.getStatus() == 206){
                retValue = 2;
            }

            if (curl.getStatus() == 201){
                update(IdCuenta, Lectura, IdAnomalia, Observa, 1);
            }
            else{
                retValue = 3;
                update(IdCuenta, Lectura, IdAnomalia, Observa, 2);
            }
        } catch (JSONException e) {
            retValue = 1;
            e.printStackTrace();
        }

        return retValue;
    }

    /**
     * Obtiene un Cursor de las Cuentas de Ruta de Lectura cargada
     *
     * @return Cursor
     */
    public Cursor getCuentas(){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c;

        c = db.rawQuery("SELECT id_cuenta, manzana, medidor FROM lecturas ORDER BY cve_loc", null);

        //db.close();

        return c;
    }

    /**
     * Obtiene un Cursor con las cuentas a Sincronizar
     *
     * @return Cursor
     */
    public Cursor getRowsSync(){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT id_cuenta, lectura, id_anomalia, id_ruta, observa FROM lecturas WHERE sync = 2", null);

        //db.close();

        return c;
    }

    /**
     * Obtiene un Cursor de la Ruta de Lectura cargada
     *
     * @return Cursor
     */
    public Cursor getAll(){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT id_cuenta, nombre, direccion, tarifa, situacion, medidor, lect_ant, id_anomalia, id_ruta, cve_loc, sync, lectura, observa FROM lecturas ORDER BY cve_loc", null);

        //db.close();

        return c;
    }

    /**
     * Obtiene un Cursor de la Ruta de Lectura por Cuenta
     *
     * @param id_cuenta int Cuenta de Usuario
     * @return Cursor
     */
    public Cursor getAll(int id_cuenta){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT id_cuenta, nombre, direccion, tarifa, situacion, medidor, lect_ant, id_anomalia, id_ruta, cve_loc, sync, lectura, observa FROM lecturas WHERE id_cuenta = " + String.valueOf(id_cuenta), null);

        //db.close();

        return c;
    }
    /**
     * Obtiene un Cursor de la tabla anomalias
     *
     * @return Cursor
     */
    public Cursor getAnomaliasAll(){
        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT id AS _id, descripcion FROM anomalias ORDER BY id_anomalia", null);

        //db.close();

        return c;
    }

    /**
     * Determina si un registro se encuentra sincronizado
     *
     * @return boolean
     */
    public boolean isSync(int IdCuenta){
        boolean retValue = false;

        DapaDb objDB = new DapaDb(appContext);

        SQLiteDatabase db = objDB.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT sync FROM lecturas WHERE id_cuenta = " + String.valueOf(IdCuenta), null);

        c.moveToFirst();

        if (c.getInt(0) == 1){
            retValue = true;
        }

        return retValue;
    }
}
