package net.yorch.dapadroid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * LectDroidDb<br>
 *
 * Crea y actualiza las tablas necesarias en SQLite<br><br>
 *
 * Copyright 2015 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0.0, 2015-09-08
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class DapaDb extends SQLiteOpenHelper {
    /**
     * DBNAME
     *
     * VAR String DBNAME Nombre de la Base de Datos SQLite
     */
    public static final String DBNAME = "dapadroid.db";

    /**
     * DEFAULT_URL
     *
     * VAR String DEFAULT_URL URL por default
     */
    public static final String DEFAULT_URL = "dapa.ddns.net:8080"; // "dapaweb.ngrok.com";

    /**
     * sqlAnomalias
     *
     * VAR String sqlAnomalias SQL para crear tabla anomalias
     */
    private String sqlAnomalias = "CREATE TABLE anomalias (id INTEGER PRIMARY KEY NOT NULL, id_anomalia INTEGER, descripcion TEXT)";

    /**
     * sqlRutas
     *
     * VAR String sqlRutas SQL para crear tabla rutas
     */
    private String sqlRutas = "CREATE TABLE rutas (id_ruta INTEGER PRIMARY KEY, id_personal INTEGER, lecturista TEXT)";

    /**
     * sqlInfoRutas
     *
     * VAR String sqlInfoRutas SQL para crear tabla informativa de rutas
     */
    private String sqlInfoRutas = "CREATE TABLE info (sb INTEGER, sector INTEGER, lecturista TEXT)";

    /**
     * sqlLecturas
     *
     * VAR String sqlLecturas SQL para crear tabla lecturas
     */
    private String sqlLecturas = "CREATE TABLE lecturas (id_cuenta INTEGER PRIMARY KEY, nombre TEXT, direccion TEXT, tarifa TEXT, situacion TEXT, medidor TEXT, " +
            "lect_ant INTEGER, id_anomalia INTEGER, id_ruta INTEGER, cve_loc TEXT, lectura INTEGER, observa TEXT, sync INTEGER, manzana INTEGER)";

    /**
     * sqlConfig
     *
     * VAR String sqlConfig SQL para crear tabla config
     */
    private String sqlConfig = "CREATE TABLE config (id INTEGER, url TEXT, key TEXT, current INTEGER, user TEXT)";

    /**
     * sqlReportes
     *
     * VAR String sqlReportes SQL para crear tabla reportes
     */
    private String sqlReportes = "CREATE TABLE reportes (id INTEGER, id_cuenta INTEGER, sb INTEGER, sector INTEGER, manzana INTEGER, direc TEXT, id_anomalia INTEGER, observa TEXT)";

    /**
     * sqlPictures
     *
     * VAR String sqlPictures SQL para crear tabla de fotos
     */
    private String sqlPictures = "CREATE TABLE pictures (id INTEGER PRIMARY KEY NOT NULL, id_reporte INTEGER, picture TEXT, sync INTEGER)";

    /**
     * sqlPicturesCta
     *
     * VAR String sqlPictures SQL para crear tabla de fotos para Lecturas
     */
    private String sqlPicturesCta = "CREATE TABLE pictures_cta (id INTEGER PRIMARY KEY NOT NULL, id_cuenta INTEGER, picture TEXT, sync INTEGER)";

    /**
     * Constructor de LectDroidDb
     *
     * @param context Context Contexto de la Aplicacion
     */
    public DapaDb(Context context) {
        super(context, DBNAME, null, 1);
    }

    /**
     * Crea las Tablas
     *
     * @param db SQLiteDatabase BD SQLite
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlAnomalias);
        db.execSQL(sqlRutas);
        db.execSQL(sqlInfoRutas);
        db.execSQL(sqlLecturas);
        db.execSQL(sqlConfig);
        db.execSQL(sqlReportes);
        db.execSQL(sqlPictures);
        db.execSQL(sqlPicturesCta);

        db.execSQL("INSERT INTO config (id, url, key, current, user) VALUES (0, '" + DEFAULT_URL + "', 'NO_KEY', 0, 'NO_USER')");
    }

    /**
     * Si hay cambios en DDL borra y vuelve a crear las tablas
     *
     * @param db SQLiteDatabase BD SQLite
     * @param oldVersion int Version Anterior
     * @param newVersion int Nueva Version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS anomalias");
        db.execSQL("DROP TABLE IF EXISTS rutas");
        db.execSQL("DROP TABLE IF EXISTS info");
        db.execSQL("DROP TABLE IF EXISTS lecturas");
        db.execSQL("DROP TABLE IF EXISTS config");
        db.execSQL("DROP TABLE IF EXISTS reportes");
        db.execSQL("DROP TABLE IF EXISTS pictures");
        db.execSQL("DROP TABLE IF EXISTS pictures_cta");

        db.execSQL(sqlAnomalias);
        db.execSQL(sqlRutas);
        db.execSQL(sqlInfoRutas);
        db.execSQL(sqlLecturas);
        db.execSQL(sqlConfig);
        db.execSQL(sqlReportes);
        db.execSQL(sqlPictures);
        db.execSQL(sqlPicturesCta);

        db.execSQL("INSERT INTO config (id, url, key, current, user) VALUES (0, '" + DEFAULT_URL + "', 'NO_KEY', 0, 'NO_USER')");
    }
}
